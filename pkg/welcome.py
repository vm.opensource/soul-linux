"""
(C) Copyright 2022 Venkatesh Mishra
file: welcome.py
author: Venkatesh Mishra
pl: Python3
Project: Soul Linux welcome app
api used: tkinter
date created: NA
time created: NA
"""

from tkinter import *
import tkinter as tk

def main():
    root = Tk()
    root.title("Soullinux greeter")
    root.geometry("500x500")
    root.mainloop()

if __name__ == "__main__":
    for i in range(1):
        main()
else:
    pass
